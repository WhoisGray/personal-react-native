import { StyleSheet } from "react-native";
import theme from "../../../theme";

export default StyleSheet.create({
  itemWrapper: {
    height: 100,
    width: "100vw",
    padding: 10,
    borderBottomWidth: 1,
    borderTopColor: "#ffe8d6",
    borderBottomColor: "#ffe8d6",
    backgroundColor: theme.color.secondary,
    elevation: 10,
  },
  itemTitle: {
    marginBottom: 8,
    color: "black",
    fontSize: 28,
    fontWeight: "bold",
  },
  itemSubTitle: {
    marginBottom: 8,
    color: "#030303",
    fontSize: 14,
  },
  itemDate: {
    color: "white",
    fontSize: 14,
    fontStyle: "italic",
  },
  action: {
    width: 100,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#e07a5f",
  },
  editAction: {
    width: 100,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f2cc8f",
  },
  actionText: {
    marginLeft: 8,
    color: "white",
    textTransform: "uppercase",
    fontSize: 14,
  },
});
