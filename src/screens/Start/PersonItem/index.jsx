import React from "react";
import { Text, TouchableNativeFeedback, View } from "react-native";
import { RectButton, Swipeable } from "react-native-gesture-handler";
import { Feather } from "@expo/vector-icons";
import styles from "./styles";

const PersonItem = ({ person, onDelete, onPress }) => {
  const SwipeAction = () => (
    <RectButton style={styles.action} onPress={onDelete}>
      <Feather name="trash-2" size={26} color="white" />
      <Text style={styles.actionText}>Delete</Text>
    </RectButton>
  );
  const SwipeEditAction = () => (
    <RectButton style={styles.editAction} onPress={onPress}>
      <Feather name="edit-2" size={26} color="white" />
      <Text style={styles.actionText}>Edit</Text>
    </RectButton>
  );

  return (
    <Swipeable
      friction={2}
      overshootLeft={false}
      overshootRight={false}
      renderLeftActions={SwipeAction}
      renderRightActions={SwipeEditAction}
    >
      <TouchableNativeFeedback onPress={onPress}>
        <View style={styles.itemWrapper}>
          <Text style={styles.itemTitle}>
            {person.PersonelName}
            <Text style={styles.itemSubTitle}> ({person.PersonelCardNo})</Text>
          </Text>

          <Text style={styles.itemDate}>
            Employ Date : {person.EmployDate.toLocaleString()}
          </Text>
        </View>
      </TouchableNativeFeedback>
    </Swipeable>
  );
};

export default PersonItem;
