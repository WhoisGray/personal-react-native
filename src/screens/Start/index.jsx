import React, { useContext, useEffect } from "react";
import { FlatList, SafeAreaView } from "react-native";
import { RectButton } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import { Feather } from "@expo/vector-icons";
import PersonItem from "./PersonItem";
import { PersonsContext } from "../../contexts";
import styles from "./styles";
import {
  axiosInstance,
  fetchPersonals,
  deletePersonal,
} from "../../utils/axios";

const Start = () => {
  const { navigate } = useNavigation();
  const { persons, destroyPerson, createPerson } = useContext(PersonsContext);
  const deleteRequest = (Id) => {
    deletePersonal(Id);

    destroyPerson(Id);
  };
  useEffect(() => {
    fetchPersonals()
      .then((e) => {
        e.map((e) => {
          createPerson(e);
        });
      })
      .catch((e) => alert(e));
    // Runs after the first render() lifecycle
  }, []);
  return (
    <SafeAreaView style={styles.startScreen}>
      <RectButton style={styles.roundedBtn} onPress={() => navigate("Form")}>
        <Feather name="plus" size={40} color="white" />
      </RectButton>

      <FlatList
        data={persons}
        keyExtractor={(item) => String(item.Id)}
        renderItem={({ item }) => (
          <PersonItem
            style={styles.personItem}
            person={item}
            onDelete={() => deleteRequest(item.Id)}
            onPress={() => navigate("Form", item.Id)}
          />
        )}
      />
    </SafeAreaView>
  );
};

export default Start;
