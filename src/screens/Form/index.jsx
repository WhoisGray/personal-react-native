import React, { useContext, useEffect, useRef, useState } from "react";
import { Text, SafeAreaView, ScrollView, View } from "react-native";
import { BorderlessButton, RectButton } from "react-native-gesture-handler";
import { useNavigation, useRoute } from "@react-navigation/native";
import { Feather } from "@expo/vector-icons";
import { PersonsContext } from "../../contexts";
import TextInput from "./TextInput";
import styles from "./styles";
import { AddPersonal, EditPersonal } from "../../utils/axios";
import SelectDropdown from "react-native-select-dropdown";

const departments = [
  "General",
  "Information Technology",
  "Accounting",
  "Marketing",
  "Human Resource",
];

const Form = () => {
  const bodyTextInput = useRef();
  const Id = useRoute().params;
  const { goBack } = useNavigation();
  const [PersonelName, setPersonelName] = useState("");
  const [PersonelCardNo, setPersonelCartNo] = useState("");
  const [EmployPhone, setEmployPhone] = useState("");
  const [EmployDepartment, setEmployDepartment] = useState("");
  const [EmployStreet, setEmployStreet] = useState("");
  const [EmployCity, setEmployCity] = useState("");
  const [EmployState, setEmployState] = useState("");
  const [EmployZip, setEmployZip] = useState("");
  const [EmployCountry, setEmployCountry] = useState("");
  const [EmployDate, setEmployDate] = useState(new Date());
  const { persons, createPerson, updatePerson } = useContext(PersonsContext);
  const placeholder = () => {
    if (EmployDepartment == "") {
      return {
        textAlign: "left",
        backgroundColor: theme.color.main,
        width: "100%",
        padding: 10,
        paddingBottom: 5,
        borderRadius: 10,
        lineHeight: 30,
        color: "#FFF",
      };
    } else {
      return {
        color: "red",
        textAlign: "left",
        backgroundColor: theme.color.main,
        width: "100%",
        padding: 10,
        paddingBottom: 5,
        borderRadius: 10,
        lineHeight: 30,
        fontStyle: "italic",
      };
    }
  };

  const handleSavePerson = () => {
    const person = {
      Id,
      PersonelName,
      PersonelCardNo,
      EmployPhone,
      EmployDepartment,
      EmployStreet,
      EmployCity,
      EmployState,
      EmployZip,
      EmployCountry,
      EmployDate,
    };
    console.log(582, person);
    var messages = "";
    for (const key in person) {
      const element = person[key];
      if (element === "") {
        messages += key + " is required.\n";
      }
    }
    if (messages.length > 0) {
      alert(messages);
    } else {
      if (Id) {
        editRequest(person);
      } else {
        createRequest(person);
      }
    }
  };

  useEffect(() => {
    if (Id) {
      const {
        PersonelName,
        PersonelCardNo,
        EmployPhone,
        EmployDepartment,
        EmployStreet,
        EmployCity,
        EmployState,
        EmployZip,
        EmployCountry,
        EmployDate,
      } = persons.find((e) => e.Id === Id); // eslint-disable-line no-shadow
      console.log(
        23,
        persons.find((e) => e.Id === Id)
      );
      console.log(EmployDepartment);
      setPersonelName(PersonelName);
      setEmployDate(EmployDate);
      setEmployPhone(EmployPhone);
      setEmployDepartment(departments.indexOf(EmployDepartment));
      setEmployStreet(EmployStreet);
      setEmployCity(EmployCity);
      setEmployState(EmployState);
      setEmployZip(EmployZip);
      setEmployCountry(EmployCountry);
      setPersonelCartNo(PersonelCardNo);
    }
  }, [Id]);

  const editRequest = (person) => {
    //TODO: remodel this
    // updatePerson(person);

    EditPersonal(person)
      .then((e) => {
        console.log(person);
        person.EmployDepartment = departments[EmployDepartment]

        updatePerson(person);
      })
      .catch((e) => alert(e))
      .finally((e) => goBack());
  };
  const createRequest = (person) => {
    // createPerson(person);

    AddPersonal(person)
      .then((e) => {
        person.EmployDepartment = departments[EmployDepartment]
        createPerson(person);
      })
      .catch((e) => alert(e))
      .finally((e) => goBack());
  };
  return (
    <SafeAreaView style={styles.formScreen}>
      <If condition={PersonelName || PersonelCardNo}>
        <RectButton style={styles.okButton} onPress={handleSavePerson}>
          <Feather name="check" size={40} color="white" />
        </RectButton>
      </If>

      <BorderlessButton style={styles.cancelButton} onPress={goBack}>
        <Feather name="x" size={30} color="white" />
      </BorderlessButton>
      <If condition={Id}>
        <Text>Customer Id :{Id} </Text>
      </If>
      <SelectDropdown
        defaultButtonText="Employ Department"
        buttonStyle={styles.dropdownButton}
        buttonTextStyle={styles.dropdownButton}
        dropdownStyle={styles.dropdownButton}
        rowTextStyle={styles.dropdownButton}
        rowStyle={styles.dropdownButton}
        data={departments}
        defaultValueByIndex={EmployDepartment}
        onSelect={(selectedItem, index) => {
          setEmployDepartment(departments.indexOf(selectedItem));
        }}
      />
      <View style={styles.separator} />
      <TextInput
        size={32}
        placeholder="Personal Name"
        onSubmitEditing={() => bodyTextInput.current.focus()}
        onChangeText={setPersonelName}
        value={PersonelName}
        autoFocus={!Id}
        blurOnSubmit={false}
      />
      <View style={styles.separator} />
      <TextInput
        size={20}
        placeholder="Personal card No"
        onChangeText={setPersonelCartNo}
        value={PersonelCardNo}
      />
      <View style={styles.separator} />

      <TextInput
        size={20}
        placeholder="Employ Phone No"
        onChangeText={setEmployPhone}
        value={EmployPhone}
      />

      <View style={styles.separator} />

      <TextInput
        size={20}
        placeholder="Employ Street"
        onChangeText={setEmployStreet}
        value={EmployStreet}
      />
      <View style={styles.separator} />

      <TextInput
        size={20}
        placeholder="Employ City"
        onChangeText={setEmployCity}
        value={EmployCity}
      />
      <View style={styles.separator} />

      <TextInput
        size={20}
        placeholder="Employ State"
        onChangeText={setEmployState}
        value={EmployState}
      />
      <View style={styles.separator} />

      <TextInput
        size={20}
        placeholder="Employ Zip"
        onChangeText={setEmployZip}
        value={EmployZip}
      />
      <View style={styles.separator} />

      <TextInput
        size={20}
        placeholder="Employ Country"
        onChangeText={setEmployCountry}
        value={EmployCountry}
      />
      <View style={styles.separator} />

      <Text style={styles.dropdownButton}>
        Employ Date :{EmployDate?.toLocaleString()}{" "}
      </Text>
    </SafeAreaView>
  );
};

export default Form;
