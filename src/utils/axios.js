import axios from "axios";

var axiosInstance = axios.create({
  baseURL: "http://localhost:8002/bsService/",
});
async function fetchPersonals() {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post("SelectPersonel")
      .then(({ data }) => {
        resolve(
          data.m_Item2.map((e) => {
            console.log("date", e.EmployDate);
            e.EmployDate = new Date(+e.EmployDate.slice(6, 19));
            return e;
          })
        );
      })
      .catch((err) => {
        reject(err);
      });
  });
}
async function deletePersonal(id) {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post("DeletePersonel", {
        id,
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
async function AddPersonal(person) {
  return new Promise((resolve, reject) => {
    var temp = JSON.parse(JSON.stringify(person));
    temp.PersonelCartNo = temp.PersonelCardNo;
    temp.EmployDepartment = +temp.EmployDepartment;
    console.log('time', temp.EmployDate);
    temp.EmployDate = `\/Date(${+new Date(temp.EmployDate)})\/`;

    delete temp.PersonelCardNo;
    axiosInstance
      .post("InsertPersonel", temp)
      .then((data) => {
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
function EditPersonal(person) {
  return new Promise((resolve, reject) => {
    var temp = JSON.parse(JSON.stringify(person));

    temp.PersonelCartNo = temp.PersonelCardNo;
    temp.EmployDepartment = +temp.EmployDepartment;
    console.log("time", temp.EmployDate);

    temp.EmployDate = `\/Date(${+new Date(temp.EmployDate)})\/`;
    temp.id = temp.Id;
    delete temp.PersonelCardNo;
    delete temp.Id;
    axiosInstance
      .post("UpdatePersonel", temp)
      .then((data) => {
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
export {
  axiosInstance,
  fetchPersonals,
  deletePersonal,
  AddPersonal,
  EditPersonal,
};
