import React from "react";
import { PersonsProvider } from "./persons";

export { PersonsContext } from "./persons";

const ContextProvider = ({ children }) => (
  <PersonsProvider>{children}</PersonsProvider>
);

export default ContextProvider;
