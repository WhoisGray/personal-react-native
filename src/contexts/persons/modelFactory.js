import idMaker from "../../utils/idMaker";

const personFactory = ({
  Id,
  PersonelName,
  PersonelCardNo,
  EmployPhone,
  EmployDepartment,
  EmployStreet,
  EmployCity,
  EmployState,
  EmployZip,
  EmployCountry,
  EmployDate,
}) => ({
  Id: Id ?? idMaker.next(),
  EmployDate: EmployDate ?? new Date(),
  PersonelName,
  EmployPhone,
  EmployDepartment,
  EmployStreet,
  EmployCity,
  EmployState,
  EmployZip,
  EmployCountry,
  PersonelCardNo,
});

export default personFactory;
