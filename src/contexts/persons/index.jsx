import React, { createContext, useReducer } from "react";
import personFactory from "./modelFactory";
import personsReducer from "./reducer";
import { mapActions } from "./actions";

const initialState = {
  persons: [
    // personFactory({ PersonelName: "Yashar", PersonelCardNo: "101" }),
    // personFactory({ PersonelName: "David", PersonelCardNo: "709" }),
    // personFactory({ PersonelName: "Tina", PersonelCardNo: "120" }),
  ],
};

export const PersonsContext = createContext(null);

export const PersonsProvider = ({ children }) => {
  const [state, dispatch] = useReducer(personsReducer, initialState);

  return (
    <PersonsContext.Provider value={{ ...state, ...mapActions(dispatch) }}>
      {children}
    </PersonsContext.Provider>
  );
};
