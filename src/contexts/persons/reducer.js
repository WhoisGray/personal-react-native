import * as mutations from "./mutations";

const personsReducer = (state, { type, payload }) => {
  try {
    return mutations[type](state, payload);
  } catch (error) {
    if (/.* is not a function$/.test(error.message)) {
      throw new Error(
        `Action type "${type}" does not exist in PersonsContext (${error.message}).`
      );
    }
  }
};

export default personsReducer;
