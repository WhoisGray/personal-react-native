import personFactory from "./modelFactory";

export const CREATE = (state, payload) => {
  const {
    Id,
    PersonelName,
    PersonelCardNo,
    EmployPhone,
    EmployDepartment,
    EmployStreet,
    EmployCity,
    EmployState,
    EmployZip,
    EmployCountry,
    EmployDate,
  } = payload;
  const newPerson = personFactory({
    Id,
    PersonelName,
    PersonelCardNo,
    EmployPhone,
    EmployDepartment,
    EmployStreet,
    EmployCity,
    EmployState,
    EmployZip,
    EmployCountry,
    EmployDate,
  });

  return { ...state, persons: [...state.persons, newPerson] };
};

export const UPDATE = (state, payload) => {
  const {
    Id,
    PersonelName,
    PersonelCardNo,
    EmployPhone,
    EmployDepartment,
    EmployStreet,
    EmployCity,
    EmployState,
    EmployZip,
    EmployCountry,
    EmployDate,
  } = payload;
  console.log(55, payload);

  const persons = state.persons.map((person) => {
    if (person.Id === Id) {
      return personFactory({
        Id,
        PersonelName,
        PersonelCardNo,
        EmployPhone,
        EmployDepartment,
        EmployStreet,
        EmployCity,
        EmployState,
        EmployZip,
        EmployCountry,
        EmployDate,
      });
      console.log(85, person);
    }
    return person;

  });

  return { ...state, persons };
};

export const DELETE = (state, personId) => {
  const persons = state.persons.filter((person) => {
    return personId !== person.Id;
  });

  return { ...state, persons };
};
