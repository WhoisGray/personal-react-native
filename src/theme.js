export default {
  color: {
    main: "#b7b7a4",
    primary: "#81b29a",
    secondary: "#cb997e",
    whitish: "#f0f0f7",
  },
};
